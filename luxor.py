#!/usr/bin/env python

##############################################################################
#
# luxor.py
#
# Python script for controlling DMX devices like RGB LED PAR cans.
# Expectes a configuration file called luxor.json in the same directory.
#
# Work in progress; feel free to implement your own Providers.
#
##############################################################################

VERSION = '1.0.0'

DEFAULT_UPDATE_INTERVAL_MS = 25.0


import colorsys
import json
import math
import random
import time
from array import *
from ola.ClientWrapper import ClientWrapper
from ola.OlaClient import Plugin
from ola.DMXConstants import DMX_MIN_SLOT_VALUE, DMX_MAX_SLOT_VALUE, DMX_UNIVERSE_SIZE


##############################################################################
#
# Provider for cycling through the H coordinate of HSB/HSL color space
#
class HueRotationProvider(object):

    # Constructor
    def __init__(self, config):
        # Memorize configuration settings
        self.base_addr = config.get('base_addr', 0)
        self._saturation = config.get('saturation', 1)
        self._brightness = config.get('brightness', 127)
        self._burst_saturation = config.get('burst_saturation', 1)
        self._burst_brightness = config.get('burst_brightness', 127)
        self._burst_cycle_duration = config.get('burst_cycle_duration', 1)
        self._burst_max_delta = config.get('burst_min_delta', 0.25)
        self._burst_min_delta = config.get('burst_max_delta', 0.75)
        self._burst_prob = config.get('burst_prob', 0)
        self._cycle_duration = config.get('cycle_duration', 60)
        self._start_hue = config.get('start_hue', 0)
        self._steps = config.get('steps', 0)
        self._steps_delta = config.get('steps_delta', 0)
        self._steps_strength = config.get('steps_strength', 1)

        # Initialize state
        self._burst_mode = False
        self._hue0 = self._start_hue
        self._running = False
        self._current_saturation = self._saturation
        self._current_brightness = self._brightness
        self._current_cycle_duration = self._cycle_duration
        self._current_hue = self._hue0

    # Starts cycling
    def Start(self):
        if not self._running:
            self._running = True
            # Remember start time
            self._t0 = time.time()

    # Called by the controller to determine the data to be sent
    # to the DMX device mapped to this provider
    def GetData(self):

        # Determine how much time has passed since last start
        # or mode change
        dt = (time.time() - self._t0)

        # If in burst mode...
        if self._burst_mode:
            # If burst is over...
            if dt >= self._burst_duration:
                # ...return to normal mode
                self._burst_mode = False
                self._hue0 = self._current_hue
                self._t0 = time.time()
                dt = 0
                self._current_cycle_duration = self._cycle_duration
                self._current_saturation = self._saturation
                self._current_brightness = self._brightness
        # If not in burst mode...
        else:
            # Calculate probability that no burst has occured since last
            # start of normal mode
            prob = (1 - math.exp(math.log(1 - self._burst_prob) * dt)) if dt > 0 else 0

            # Roll the dice to determine whether a burst
            # should be initiated. If so...
            if random.random() < prob:
                # ...switch to burst mode
                self._burst_mode = True

                # Choose burst duration
                dburst = self._burst_min_delta + random.random() * (self._burst_max_delta - self._burst_min_delta)
                self._burst_duration = dburst * self._burst_cycle_duration

                self._hue0 = self._current_hue
                self._t0 = time.time()
                dt = 0
                self._current_cycle_duration = self._burst_cycle_duration
                self._current_saturation = self._burst_saturation
                self._current_brightness = self._burst_brightness

        # Create DMX data
        data = array('B')
        # First byte: brightness
        data.append(self._current_brightness)
        # Base hue value
        hue = self._hue0 + dt/self._current_cycle_duration
        # Calculate step modulation
        dh = - self._steps_strength * 0.5 * math.sin(2 * self._steps * math.pi * (hue - self._steps_delta)) / (3 * self._steps) if self._steps != 0 else 0
        # Actual current hue
        self._current_hue = (hue + dh) % 1
        # Add RGB values to DMX data
        data.extend(int(x * DMX_MAX_SLOT_VALUE) for x in colorsys.hls_to_rgb(self._current_hue, 0.5, self._current_saturation))

        return data

    # Stops cycling
    def Stop(self):
        self._running = False


##############################################################################
#
# Controller, which periodically polls all providers for the DMX data to
# be sent to their associated DMX device.
#
class Controller(object):

    # Constructor
    def __init__(self, config, providers):
        # Memorize the list of providers
        self._providers = providers

        # Memorize configuration settings
        self._universe = config.get('universe', 0)
        self._update_interval = config.get('update_interval', DEFAULT_UPDATE_INTERVAL_MS)

        # Initialize state
        self._running = False

        # OLA initialization
        self._wrapper = ClientWrapper()

    # Start the controller
    def Start(self):
        if not self._running:
            self._running = True
            # Start all providers
            for provider in self._providers:
                provider.Start()
            # Prepare OLA client wrapper for first call of update callback
            self._wrapper.AddEvent(0, self.Update)
            # Start OLA client wrapper
            self._wrapper.Run()

    # Called by OLA client wrapper as scheduled event callback
    def Update(self):
        # Array for holding all DMX data
        data = array('B')
        i = 0
        # Iterate through all providers
        for provider in self._providers:
            # Fill gaps in DMX address space with null values
            while i < provider.base_addr:
                data.append(0)
                i += 1
            # Get and append the current providers current values
            pd = provider.GetData()
            data.extend(pd)
            i += len(data)
        # Send the whole bunch of DMX data
        self._wrapper.Client().SendDmx(self._universe, data)
        # Prepare wrapper for the next update cycle
        self._wrapper.AddEvent(self._update_interval, self.Update)

    # Stop the controller
    def Stop(self):
        for provider in self._providers:
            provider.Stop()
        self.wrapper.Stop()
        self._running = False


##############################################################################
#
# Global suroutines
#
##############################################################################

# Read configuration and convert from JSON
def read_config():
    data = open('luxor.json').read()
    return json.loads(data)

# Create provider instances
#
# N.B Currently only the above HueRotationProvider is supported.
# This is the place where you want make your own providers
# known to the controller.
#
def create_providers(configs):
    providers = []
    for config in sorted(configs, key=lambda config: config.get('base_addr')):
        if config.get('type') == 'hue_rotation':
            provider = HueRotationProvider(config)
            providers.append(provider)
    return providers


##############################################################################
#
# Main program
#
##############################################################################

if __name__ == '__main__':

    config = read_config()
    providers = create_providers(config.get('providers', []))

    controller = Controller(config.get('controller', {}), providers)
    controller.Start()

    # Keep running until CTRL+C
    try:
        while True:
            1
    except KeyboardInterrupt:
        print('Interrupted!')

    controller.Stop()
